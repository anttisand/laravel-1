<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'slug', 'body'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function addComment($body)
    {
        return $this->comments()->create(['body' => $body]);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function getStats()
    {
        return self::selectRaw('monthname(created_at) as month, 
            year(created_at) as year, 
            count(*) as count')
            ->groupBy('year', 'month')
            ->get();
    }
}
