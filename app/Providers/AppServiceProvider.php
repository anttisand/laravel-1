<?php

namespace App\Providers;

use App\Post;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.sidebar', function($view) {
            $view->with('stats', Post::getStats());
        });

        Validator::extend('onlyantti', function($attribute, $value, $parameters, $validator) {
            return $value == 'antti';
        });

        Validator::extend('startswith', function($attribute, $value, $parameters, $validator) {
            return strtolower($value[0]) == $parameters[0];
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
