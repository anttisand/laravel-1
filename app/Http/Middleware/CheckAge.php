<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(($user->country == 'fi' && $user->age < 18)
            ||($user->country == 'us') && $user->age < 21) {
            return redirect('/')
                ->with('status', 'You are not old enough to see that!');
        }

        return $next($request);
    }
}
