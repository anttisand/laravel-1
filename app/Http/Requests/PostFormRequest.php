<?php

namespace App\Http\Requests;

use App\Post;
use App\Tag;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('add-post', Auth::user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2',
            'body' => 'required',
            'newtag' => 'required_without:tags'
        ];
    }

    public function persist()
    {
        $post = Post::create([
            'title' => $this->title,
            'slug' => str_slug($this->title),
            'body' => $this->body,
        ]);

        if(!is_null($this->newtag)) {
            $tag = Tag::create([
                'name' => $this->newtag
            ]);

            $post->tags()->attach($tag);
        }

        if(isset($this->tags)) {
            $post->tags()->attach($this->tags);
        }
    }
}
