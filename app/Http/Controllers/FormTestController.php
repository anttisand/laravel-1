<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormTestController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha|min:2|onlyantti',
            'age' => 'required|numeric',
        ]);

        $validator->sometimes('age', 'min:18', function($input) {
            return $input->country == 'fi';
        });

        $validator->sometimes('age', 'min:21', function($input) {
            return $input->country == 'us';
        });

        if($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        return "Meni läpi";
    }
}
