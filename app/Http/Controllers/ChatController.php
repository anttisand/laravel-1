<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Events\ChatMessageSent;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        return view('chat');
    }

    public function store(Request $request)
    {
        $message = ChatMessage::create([
            'user_id' => auth()->id(),
            'message' => $request->body
        ]);

        event(new ChatMessageSent(auth()->user(), $message));
    }
}
