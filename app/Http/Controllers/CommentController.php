<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    public function index($id)
    {
        return Comment::where('post_id', $id)->get();
    }

    public function save($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required|min:5'
        ]);

        $post = Post::find($id);
        $comment = $post->addComment($request->body);

        if($request->wantsJson()) {
            return response([
                'message' => 'Comment was created',
                'id' => $comment->id,
            ], 200);
        }

        return back()->with('status', 'Comment was added');
    }

    public function delete($id, Request $request)
    {
        if(Gate::denies('remove-comment', $id)) {
            $message = 'You don\'t have the necessary role to do that';

            if($request->wantsJson()) {
                return response($message, 403);
            }

            return back()
                ->with('status', $message);
        }

        Comment::destroy($id);

        $message = 'Comment was removed';
        if($request->wantsJson()) {
            return response($message, 200);
        }

        return back()
            ->with('status', $message);
    }
}
