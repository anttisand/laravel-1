<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostFormRequest;

use App\Post;
use App\Tag;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->get();

        return view('posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        $tags = Tag::all();
        return view('posts.create', compact('tags'));
    }

    public function save(PostFormRequest $request)
    {
        $request->persist();

        return back()->with('status', 'Post was created');
    }
}
