<?php

Route::get('/comments/{id}', 'CommentController@index');
Route::get('/posts/{post}', 'PostsController@show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/posts/create', 'PostsController@create');
    Route::post('/posts', 'PostsController@save');
    Route::delete('/comments/{id}', 'CommentController@delete');

    Route::get('chat', 'ChatController@index');
    Route::post('chat', 'ChatController@store');
});

Route::group(['middleware' => ['auth', 'adultsOnly']], function () {
    Route::post('/posts/{id}/comments', 'CommentController@save');
});

Route::get('/form', 'FormTestController@index');
Route::post('/form', 'FormTestController@store');

Route::get('/', 'PostsController@index');

Auth::routes();

