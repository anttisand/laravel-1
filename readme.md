# Laravel 1

Ensimmäisellä luennolla käytiin läpi tietomallit, kontrollerit, reititys ja master template.

Niihin liittyvät muutokset ovat lähinnä `database/migrations`, `Post` ja `Comment` luokat, `routes/web`, `resources/views/layout ja posts`.

Huomioi Post ja Comment -tietomallien relaatiometodit `post->comments` ja `comment->post`.

Huomioi templateista master template `layouts/app.blade.php` ja sen:

    @yield('kohta') - tähän voi liittää
    @extends('layouts.app') - tätä käytetään pohjana
    @section('kohta') - kohta, joka liitetään
    @endsection
    @include('joku') - otetaan tähän kohtaan mukaan

Blade komentoja:

    @if() - @elseif() - @else
    @foreach() - $endforeach
    @forelse() - @empty - @endforelse
    @unless()
