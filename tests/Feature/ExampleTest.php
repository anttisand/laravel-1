<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;

    public function testSiteAvailable()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testAddingPostWithoutSigningIn()
    {
        $response = $this->post('/posts');

        $response->assertRedirect('/login');
    }

    public function testAddingPostWithoutTags()
    {
        Auth::loginUsingId(3);

        $response = $this->post('/posts', [
            'title' => 'testpost',
            'body' => 'testpost',
            '_token' => csrf_token()
        ]);

        $response->assertSessionHasErrors(['newtag']);
    }

    public function testAddingPostWithTags()
    {
        Auth::loginUsingId(3);

        $response = $this->post('/posts', [
            'title' => 'testpost',
            'body' => 'testpost',
            'newtag' => 'fuu',
            '_token' => csrf_token()
        ]);

        $response->assertSessionHas('status', 'Post was created');
    }
}
