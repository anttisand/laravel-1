
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.bus = new Vue();

window.NotificationStore = {
    state: [],
    addNotification(notification) {
        this.state.push(notification);
    },
    removeNotification(notification) {
        let index = this.state.indexOf(notification);
        this.state.splice(index,1);
    }
};

Vue.component('public-chat', require('./components/PublicChat.vue'));
Vue.component('notifications', require('./components/Notifications.vue'));
Vue.component('comment-list', require('./components/CommentsList.vue'));
Vue.component('comment-form', require('./components/CommentForm.vue'));
Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});
