<!-- Blog Sidebar Widgets Column -->
<div class="col-md-4">

    <!-- Side Widget Well -->
    <div class="well">
        <h4>Side Widget Well</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Articles by month</h4>
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">
                    @forelse($stats as $stat)
                        <li>{{$stat->month}} {{$stat->year}} ({{$stat->count}})</li>
                   @empty
                    @endforelse
                </ul>
            </div>
        </div>
        <!-- /.row -->
    </div>

</div>