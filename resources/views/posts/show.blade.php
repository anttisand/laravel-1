@extends('layouts.app')

@section('content')
    <div class="col-lg-8">
        <h1>{{$post->title}}</h1>
        <hr>
        <p>Posted {{$post->created_at->diffForHumans()}}</p>
        <hr>

        <p class="lead">{{$post->body}}</p>

        <comment-list
                postid="{{(int)$post->id}}"
                candelete="{{var_export(Gate::allows('remove-comment'), true)}}">
        </comment-list>

        @if(Auth::check())
            <comment-form postid="{{$post->id}}"></comment-form>
        @else
            Please sign in to post comments
        @endif
    </div>

    @include('layouts.sidebar')
@endsection