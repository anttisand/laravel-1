@extends('layouts.app')

@section('content')
    <div class="col-lg-8">
        <form action="/posts" method="post">
            {{csrf_field()}}

            @foreach($tags as $tag)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="tags[]" value="{{$tag->id}}">
                        {{$tag->name}}
                    </label>
                </div>
            @endforeach

            <div class="form-group {{ $errors->has('newtag') ? 'has-error' : '' }}">
                <label class="control-label" for="newtag">Luo uusi luokka:</label>
                <input class="form-control" type="text" name="newtag" id="newtag" value="{{old('newtag')}}">
                {!!$errors->first('newtag', '<p class="help-block">:message</p>')!!}
            </div>

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label class="control-label" for="title">Title:</label>
                <input class="form-control" type="text" name="title" id="title" value="{{old('title')}}">
                {!!$errors->first('title', '<p class="help-block">:message</p>')!!}
            </div>

            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                <label class="control-label" for="body">Contents:</label>
                <textarea class="form-control" name="body" id="body">{{old('body')}}</textarea>
                {!!$errors->first('body', '<p class="help-block">:message</p>')!!}
            </div>

            <button type="submit" class="btn btn-primary">Add a new post</button>
        </form>
    </div>

    @include('layouts.sidebar')

@endsection