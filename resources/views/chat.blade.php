@extends('layouts.app')

@section('content')
    <div class="col-lg-8">
        <public-chat></public-chat>
    </div>

    @include('layouts.sidebar')

@endsection