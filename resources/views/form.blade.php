@extends('layouts.app')

@section('content')
    <div class="col-lg-8">
        <form action="/form" method="post">
            {{csrf_field()}}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label class="control-label" for="name">Name:</label>
                <input class="form-control" type="text" name="name" id="name" value="{{old('name')}}">
                {!!$errors->first('name', '<p class="help-block">:message</p>')!!}
            </div>

            <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                <label class="control-label" for="age">Age:</label>
                <input class="form-control" type="text" name="age" id="age" value="{{old('age')}}">
                {!!$errors->first('age', '<p class="help-block">:message</p>')!!}
            </div>

            <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                <label class="control-label" for="country">Country:</label>
                <select name="country" id="country" class="form-control">
                    <option value="fi">Finland</option>
                    <option value="us">USA</option>
                </select>
                {!!$errors->first('country', '<p class="help-block">:message</p>')!!}
            </div>

            <button type="submit" class="btn btn-primary">Validate this form</button>
        </form>
    </div>

    @include('layouts.sidebar')

@endsection